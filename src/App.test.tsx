import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { useGithubData } from './App';
import { GithubData } from './api';
import { RequestAPI } from './utils/request';

const testResData: GithubData = {"current_user_url":"https://api.github.com/user","current_user_authorizations_html_url":"https://github.com/settings/connections/applications{/client_id}","authorizations_url":"https://api.github.com/authorizations","code_search_url":"https://api.github.com/search/code?q={query}{&page,per_page,sort,order}","commit_search_url":"https://api.github.com/search/commits?q={query}{&page,per_page,sort,order}","emails_url":"https://api.github.com/user/emails","emojis_url":"https://api.github.com/emojis","events_url":"https://api.github.com/events","feeds_url":"https://api.github.com/feeds","followers_url":"https://api.github.com/user/followers","following_url":"https://api.github.com/user/following{/target}","gists_url":"https://api.github.com/gists{/gist_id}","hub_url":"https://api.github.com/hub","issue_search_url":"https://api.github.com/search/issues?q={query}{&page,per_page,sort,order}","issues_url":"https://api.github.com/issues","keys_url":"https://api.github.com/user/keys","label_search_url":"https://api.github.com/search/labels?q={query}&repository_id={repository_id}{&page,per_page}","notifications_url":"https://api.github.com/notifications","organization_url":"https://api.github.com/orgs/{org}","organization_repositories_url":"https://api.github.com/orgs/{org}/repos{?type,page,per_page,sort}","organization_teams_url":"https://api.github.com/orgs/{org}/teams","public_gists_url":"https://api.github.com/gists/public","rate_limit_url":"https://api.github.com/rate_limit","repository_url":"https://api.github.com/repos/{owner}/{repo}","repository_search_url":"https://api.github.com/search/repositories?q={query}{&page,per_page,sort,order}","current_user_repositories_url":"https://api.github.com/user/repos{?type,page,per_page,sort}","starred_url":"https://api.github.com/user/starred{/owner}{/repo}","starred_gists_url":"https://api.github.com/gists/starred","user_url":"https://api.github.com/users/{user}","user_organizations_url":"https://api.github.com/user/orgs","user_repositories_url":"https://api.github.com/users/{user}/repos{?type,page,per_page,sort}","user_search_url":"https://api.github.com/search/users?q={query}{&page,per_page,sort,order}"};

const TestApp = (props: { api: RequestAPI<GithubData> }) => {
  const { loadData, currentData, history, isLoading } = useGithubData(props.api);
  return <div>
    <div data-testid="loading">{isLoading ? 1 : 0}</div>
    <div data-testid="currentData">{JSON.stringify(currentData)}</div>
    <div data-testid="his">{history.map((it) => <div key={it.startTime}>
      {it.startTime} - status:{it.success ? 1 : 0}
    </div>)}</div>
    <button data-testid="load" onClick={loadData}></button>
  </div>;
};

test('App 请求github并记录日志的逻辑测试', async () => {
  let success = true;
  const api = async () => {
    if (success) {
      return testResData;
    } else {
      throw new Error('');
    }
  }
  render(<TestApp api={api} />);
  expect(screen.getByTestId('loading')).toHaveTextContent('0');
  expect(screen.getByTestId('currentData')).toHaveTextContent('null');
  expect(screen.getByTestId('his').children.length).toBe(0);

  fireEvent.click(screen.getByTestId('load'));
  await waitFor(() => screen.getByTestId('loading').textContent === '0');

  expect(screen.getByTestId('currentData')).toHaveTextContent('"current_user_url":"https://api.github.com/user"');
  expect(screen.getByTestId('his').children.length).toBe(1);

  // 成功请求
  fireEvent.click(screen.getByTestId('load'));
  await waitFor(() => screen.getByTestId('loading').textContent === '0');
  expect(screen.getByTestId('his').children.length).toBe(2);
  expect(screen.getByTestId('his').children[1]).toHaveTextContent('status:1');
  
  // 失败请求测试
  success = false;
  fireEvent.click(screen.getByTestId('load'));
  await waitFor(() => screen.getByTestId('loading').textContent === '0');
  expect(screen.getByTestId('his').children.length).toBe(3);
  expect(screen.getByTestId('his').children[2]).toHaveTextContent('status:0');
});

