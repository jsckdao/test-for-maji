import { APIError, defineAPI } from './request';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import Url from 'url';

const server = setupServer(
  rest.get('/test', (req, res, ctx) => {
    return res(ctx.json({ ...Url.parse(req.url.toString(), true).query, greeting: 'hello there' }))
  }),
  rest.get('/error401', (req, res, ctx) => {
    return res(ctx.status(401), ctx.json({ message: 'auth error'}), );
  })
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('request 正常测试', async () => {
  const api = defineAPI<any>('/test');
  const data = await api({ a: 'a', b: 'b' });
  expect(data.a).toBe('a');
  expect(data.b).toBe('b');
  expect(data.greeting).toBe('hello there');
});

test('request 错误测试', async () => {
  const api = defineAPI<any>('/error401');
  const res = await api({ a: 'a', b: 'b' }).then((res) => res, (err) => err);
  expect(res).toBeInstanceOf(APIError);
  expect(res.code).toBe(401);
  expect(res.message).toBe('请求失败: auth error');
});