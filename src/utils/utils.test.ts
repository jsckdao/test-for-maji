import { timeFormat, timePartFormat } from './utils';

test('时间格式化', () => {
  expect(timeFormat(1605948040046)).toBe('16:40:40');
});

test('秒数格式化', () => {
  expect(timePartFormat(333932)).toBe('333.93s');
})