
export type RequestAPI<R, P = {}> = (params?: P) => Promise<R>;

export interface APIOption {
  method?: 'GET' | 'POST' | 'PUT' | 'HEAD' | 'DELETE' | 'OPTION';
  headers?: { [k:string]: string };
}

/**
 * API 请求异常
 */
export class APIError extends Error {
  constructor(message: string, public readonly code: number) {
    super(message);
  }
}

/**
 * 定义一个服务端 API 请求
 * @param url 请求地址
 * @param op 其他配置项
 */
export function defineAPI<R, P = {}>(url: string, option: APIOption = {}): RequestAPI<R, P> {
  const { method = 'GET', headers = {} } = option;
  return async (params?) => {
    try {
      const finalUrl = method === 'GET' ? url + stringifyParams(params): url;
      const body = method === 'GET' ? '' : JSON.stringify(params || {});
      const response = await fetch(finalUrl, {
        method,
        mode: 'cors',
        body: body || undefined,
        headers: {
          ...headers,
          'Content-Type': 'application/json'
        }
      });
      if (response.ok) {
        // 请求成功, 获取数据
        return (await response.json()) as R;
      } else {
        // 请求失败, 获取服务器端返回的错误原因, 抛出异常
        const { message } = await response.json();
        throw new APIError('请求失败: ' + message, response.status);
      }
    } catch (err) {
      if (err instanceof APIError) {
        throw err;
      } else {
        // 处理其他暂无法预料的错误
        throw new APIError('请求失败: ' + err.message, 600);
      }
    }
  }
}

function stringifyParams(params?: any) {
  if (!params) {
    return ''
  }
  return '?' + Object.keys(params)
    .map((k) => k + '=' + encodeURIComponent(params[k]))
    .join('&');
}