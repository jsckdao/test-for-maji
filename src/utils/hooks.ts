import { useEffect, useRef } from "react";

/**
 * hook: 在组件中使用定时器, 组件卸载时, 定时器自动清除
 * @param process 定时器任务
 * @param time 周期, 为0时定时器会自动停止
 */
export function useInterval(process: () => void, time = 0) {
  const processRef = useRef<Function>();
  processRef.current = process;
  useEffect(() => {
    const run = () => processRef.current && processRef.current();
    const handle = time > 0 ? setInterval(run, time) : null;
    if (handle) {
      return () => clearInterval(handle);
    }
  }, [time]);
} 