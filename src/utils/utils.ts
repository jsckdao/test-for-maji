/**
 * 秒数格式化(秒)
 * @param s 毫秒数
 */
export function timePartFormat(s: number) {
  return (s / 1000).toFixed(2) + 's';
}


/**
 * 时间格式化
 * @param d 毫秒数
 */
export function timeFormat(d: number) {
  const time = new Date(d);
  return [time.getHours(), time.getMinutes(), time.getSeconds()].join(':');
}