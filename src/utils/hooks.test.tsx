import { render, screen, fireEvent } from '@testing-library/react';
// import jest from '@testing-library/jest-dom';
import React, { useState } from "react";
import { useInterval } from './hooks';

// jest.useFakeTimers();

const App = ({ onRun = () => {} }) => {
  const [time, setTime] = useState(100);
  useInterval(onRun, time);
  return <div>
    <button data-testid="btn-500" onClick={() => setTime(500)}></button>
    <button data-testid="btn-0" onClick={() => setTime(0)}></button>
  </div>;
}

beforeEach(() => {
  jest.useFakeTimers()
});

afterEach(() => {
  jest.runOnlyPendingTimers()
  jest.useRealTimers()
})

test('定时器hook 正常测试', async () => {
  const onRun = jest.fn();
  render(<App onRun={onRun} />);

  // 正常计算测试
  jest.advanceTimersByTime(760);
  expect(onRun).toHaveBeenCalledTimes(7);
  jest.advanceTimersByTime(70);
  expect(onRun).toHaveBeenCalledTimes(8);
  jest.advanceTimersByTime(110);
  expect(onRun).toHaveBeenCalledTimes(9);
  // 变更周期
  fireEvent.click(screen.getByTestId('btn-500'));
  jest.advanceTimersByTime(200);
  expect(onRun).toHaveBeenCalledTimes(9);
  jest.advanceTimersByTime(310);
  expect(onRun).toHaveBeenCalledTimes(10);
  // 暂停计时
  fireEvent.click(screen.getByTestId('btn-0'));
  jest.advanceTimersByTime(2000);
  jest.advanceTimersByTime(20000);
  expect(onRun).toHaveBeenCalledTimes(10);
});