import { getGithubData } from './index';

test("api 接口测试", async () => {
  const result = await getGithubData();
  expect(result.current_user_url).toBe('https://api.github.com/user');
  expect(result.authorizations_url).toBe('https://api.github.com/authorizations');
});