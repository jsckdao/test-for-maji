import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from "react-router-dom";
import "./App.css";
import { RequestAPI } from "./utils/request";
import { getGithubData, GithubData } from "./api";
import { useInterval } from "./utils/hooks";
import { timeFormat, timePartFormat } from "./utils/utils";

interface HistoryItem {
  startTime: number;
  totalTime: number;
  success: boolean;
}

/**
 * 加载数据, 添加记录主逻辑
 * @param api
 */
export function useGithubData(api: RequestAPI<GithubData>) {
  const [currentData, setCurrentData] = useState(null as GithubData | null);
  const [history, setHistory] = useState([] as Array<HistoryItem>);
  const [isLoading, setLoading] = useState(false);
  // 添加记录
  const addHistory = (startTime: number, success = true) => {
    setHistory(
      history.concat({
        success,
        startTime,
        totalTime: new Date().getTime() - startTime,
      })
    );
  };
  // 加载数据
  const load = async () => {
    const startTime = new Date().getTime();
    try {
      setLoading(true);
      setCurrentData(await api());
      addHistory(startTime);
    } catch (err) {
      addHistory(startTime, false);
    } finally {
      setLoading(false);
    }
  };
  return {
    currentData,
    history,
    isLoading,
    loadData: () => {
      load();
    },
  };
}

type GithubDataKeys = keyof GithubData;

const IndexPage: React.FC<{ data: GithubData | null }> = ({ data }) => {
  if (!data) {
    return <div>等待加载...</div>;
  }
  const keys = Object.keys(data) as GithubDataKeys[];
  return (
    <div className="index">
      <table>
        <tbody>
          {keys.map((k) => (
            <tr key={k}>
              <td>{k}</td>
              <td>{data[k]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

const HistoryPage: React.FC<{ data: HistoryItem[] }> = ({ data }) => {
  return (
    <div className="history">
      <table>
        <thead>
          <tr>
            <th>调用时间</th>
            <th>加载时间</th>
            <th>状态</th>
          </tr>
        </thead>
        <tbody>
          {data.map((it) => (
            <tr key={it.startTime}>
              <td>{timeFormat(it.startTime)}</td>
              <td>{timePartFormat(it.totalTime)}</td>
              <td>{it.success ? "成功" : "失败"}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

function App() {
  const { loadData, currentData, history, isLoading } = useGithubData(
    getGithubData
  );

  useInterval(loadData, 5000);

  return (
    <div className="App">
      <Router>
        <header>
          <div>
            <NavLink to="/" exact={true} activeClassName="active">
              首页
            </NavLink>
            <NavLink to="/history" activeClassName="active">
              历史
            </NavLink>
          </div>
          {isLoading && <div className="loading">Loading...</div>}
        </header>
        <Switch>
          <Route path="/" exact={true}>
            <IndexPage data={currentData} />
          </Route>
          <Route path="/history">
            <HistoryPage data={history} />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
